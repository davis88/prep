(function () {
    angular
        .module("CUSTApp")
        .controller("CustomerController", CustomerController)
        .controller("EditCustomerCtrl", EditCustomerCtrl)
        .controller("AddCustomerCtrl", AddCustomerCtrl)
        .controller("DeleteCustomerCtrl", DeleteCustomerCtrl);
        
    CustomerController.$inject = ['CUSTAppAPI', '$uibModal', '$document', '$scope'];
    EditCustomerCtrl.$inject = ['$uibModalInstance', 'CUSTAppAPI', 'items', '$rootScope', '$scope'];
    AddCustomerCtrl.$inject = ['$uibModalInstance', 'CUSTAppAPI', 'items', '$rootScope', '$scope'];
    DeleteCustomerCtrl.$inject = ['$uibModalInstance', 'CUSTAppAPI', 'items', '$rootScope', '$scope'];
    
    function DeleteCustomerCtrl($uibModalInstance, CUSTAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteCustomer = deleteCustomer;
        console.log(items);
        CUSTAppAPI.getCustomer(items).then((result)=>{
            console.log(result.data);
            self.customer =  result.data;
            self.customer.birth_date = new Date( self.customer.birth_date);
            self.customer.join_date = new Date( self.customer.join_date);
            console.log(self.customer.birth_date);
        });

        function deleteCustomer(){
            console.log("delete customer ...");
            CUSTAppAPI.deleteCustomer(self.customer.cust_no).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshCustomerList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }

    }

    function AddCustomerCtrl($uibModalInstance, CUSTAppAPI, items, $rootScope, $scope){
        console.log("Add Customer");
        var self = this;
        self.saveCustomer = saveCustomer;

        self.customer = {
            gender: "M"
        }
        initializeCalendar($scope);
        function saveCustomer(){
            console.log("save customer ...");
            console.log(self.customer.first_name);
            console.log(self.customer.last_name);
            console.log(self.customer.join_date);
            console.log(self.customer.birth_date);
            console.log(self.customer.gender);
            CUSTAppAPI.addCustomer(self.customer).then((result)=>{
                //console.log(result);
                console.log("Add customer -> " + result.cust_no);
                $rootScope.$broadcast('refreshCustomerListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                self.errorMessage = error;
             })
            $uibModalInstance.close(self.run);
        }
    }
        
    function initializeCalendar($scope){
        self.datePattern = /^\d{4}-\d{2}-\d{2}$/;;
        
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
    
        $scope.clear = function() {
            $scope.dt = null;
        };
    
        $scope.inlineOptions = {
            customClass: getDayClass,
            minDate: new Date(),
            showWeeks: true
        };
    
        $scope.dateOptions = {
            //dateDisabled: disabled,
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
        };

        // Disable weekend selection
        function disabled(data) {
            console.log(data);
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }
    
        $scope.toggleMin = function() {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
        };
    
        $scope.toggleMin();
    
        $scope.open1 = function() {
            $scope.popup1.opened = true;
        };

        $scope.open2 = function() {
            $scope.popup2.opened = true;
        };
    
        $scope.setDate = function(year, month, day) {
            $scope.dt = new Date(year, month, day);
        };
    
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[1];
        $scope.altInputFormats = ['M!/d!/yyyy'];
    
        $scope.popup1 = {
            opened: false
        };

        $scope.popup2 = {
            opened: false
        };
    
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var afterTomorrow = new Date();
        afterTomorrow.setDate(tomorrow.getDate() + 1);
        $scope.events = [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
        ];
    
        function getDayClass(data) {
            var date = data.date,
                mode = data.mode;
            if (mode === 'day') {
                var dayToCheck = new Date(date).setHours(0,0,0,0);
        
                for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
        
                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
                }
            }
        
            return '';
        }
    }

    function EditCustomerCtrl($uibModalInstance, CUSTAppAPI, items, $rootScope, $scope){
        console.log("Edit Customer Ctrl");
        var self = this;
        self.items = items;
        initializeCalendar($scope);

        CUSTAppAPI.getCustomer(items).then((result)=>{
           console.log(result.data);
           self.customer =  result.data;
           self.customer.birth_date = new Date( self.customer.birth_date);
           self.customer.join_date = new Date( self.customer.join_date);
           console.log(self.customer.birth_date);
        })

        self.saveCustomer = saveCustomer;

        function saveCustomer(){
            console.log("save customer ...");
            console.log(self.customer.first_name);
            console.log(self.customer.last_name);
            console.log(self.customer.join_date);
            console.log(self.customer.birth_date);
            console.log(self.customer.gender);
            CUSTAppAPI.updateCustomer(self.customer).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshCustomerList');
             }).catch((error)=>{
                console.log(error);
             })
            $uibModalInstance.close(self.run);
        }

    }

    function CustomerController(CUSTAppAPI, $uibModal, $document, $scope) {
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.customers = [];
        self.maxsize=5;
        self.totalItems = 0;
        self.itemsPerPage = 10;
        self.currentPage = 1;

        self.searchCustomer =  searchCustomer;
        self.addCustomer =  addCustomer;
        self.editCustomer = editCustomer;
        self.deleteCustomer = deleteCustomer;
        self.pageChanged = pageChanged;

        function searchAllCustomers(searchKeyword,orderby,itemsPerPage,currentPage){
            CUSTAppAPI.searchCustomer(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                self.customers = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems /self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });
        }


        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllCustomers(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshCustomerList",function(){
            console.log("refresh customer list "+ self.searchKeyword);
            searchAllCustomers(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshCustomerListFromAdd",function(event, args){
            console.log("refresh customer list from cust_no"+ args.cust_no);
            var customers = [];
            customers.push(args);
            self.searchKeyword = "";
            self.customers = customers;
        });

        function searchCustomer(){
            console.log("search customer  ....");
            console.log(self.orderby);
            searchAllCustomers(self.searchKeyword, self.orderby,self.itemsPerPage, self.currentPage);
        }

        function addCustomer(size, parentSelector){
            console.log("post add customer  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/addCustomer.html',
                controller: 'AddCustomerCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }

        function editCustomer(cust_no, size, parentSelector){
            console.log("Edit Customer...");
            console.log("cust_no > " + cust_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/editCustomer.html',
                controller: 'EditCustomerCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return cust_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function deleteCustomer(cust_no, size, parentSelector){
            console.log("delete Customer...");
            console.log("cust_no > " + cust_no);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './app/deleteCustomer.html',
                controller: 'DeleteCustomerCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return cust_no;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }
    }
})();