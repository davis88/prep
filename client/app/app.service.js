(function(){
    angular
        .module("CUSTApp")
        .service("CUSTAppAPI", [
            '$http',
            CUSTAppAPI
        ]);
    
    function CUSTAppAPI($http){
        var self = this;

        // query string
        self.searchCustomer = function(value, sortby, itemsPerPage, currentPage){
            return $http.get(`/api/customers?keyword=${value}&sortby=${sortby}&itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`);
        }

        self.getCustomer = function(cust_no){
            console.log(cust_no);
            return $http.get("/api/customers/" + cust_no)
        }

        self.updateCustomer = function(customer){
            console.log(customer);
            return $http.put("/api/customers",customer);
        }

        self.deleteCustomer = function(cust_no){
            console.log(cust_no);
            return $http.delete("/api/customers/"+ cust_no);
        }
        
        // parameterized values
        /*
        self.searchCustomer = function(value){
            return $http.get("/api/customers/" + value);
        }*/

        // post by body over request
        self.addCustomer = function(customer){
            return $http.post("/api/customers", customer);
        }
    }
})();