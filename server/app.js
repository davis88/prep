var express = require("express");
var bodyParser = require('body-parser');
var Sequelize = require ("sequelize");

var node_port = process.env.PORT || 3000;
const EMPLOYEE_API = "/api/customers";

//console.log(Sequelize);
const SQL_USERNAME="root";
const SQL_PASSWORD="88YouYou"
var connection = new Sequelize(
    'customers',
    SQL_USERNAME,
    SQL_PASSWORD,
    {
        host: 'localhost',
        port: 3306,
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 10,
            min: 0,
            idle: 20000,
            acquire: 20000
        }
    }
);

var Customers = require('./models/customers')(connection, Sequelize);

var app = express();
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(express.static(__dirname + "/../client/"));

// create
app.post(EMPLOYEE_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    var customer = req.body;
    customer.join_date= new Date(customer.join_date);
    customer.birth_date= new Date(customer.birth_date);
    console.log("customer gender :" + customer.gender);
    Customers.create(customer).then((result)=>{
        console.log(result);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });
});

// update
app.put(EMPLOYEE_API, (req, res)=>{
    console.log(">>> " + JSON.stringify(req.body));
    console.log("update customer ..." + req.body);
    console.log(req.body.cust_no);
    var cust_no = req.body.cust_no;
    console.log(cust_no);
    var whereClause = {limit: 1, where: {cust_no: cust_no}};
    Customers.findOne(whereClause).then((result)=>{
        result.update(req.body);
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

// retrieve 
app.get(EMPLOYEE_API, (req, res)=>{
    console.log("search > " + req.query.keyword);
    var keyword = req.query.keyword;
    var sortby = req.query.sortby;
    var itemsPerPage = parseInt(req.query.itemsPerPage);
    var currentPage = parseInt(req.query.currentPage);
    var offset = (currentPage - 1) * itemsPerPage;
    if(sortby == 'null'){
        console.log("sortby is null");
        sortby = "ASC";
    }
    console.log(keyword);
    console.log(itemsPerPage);
    console.log(currentPage);
    console.log(typeof offset);
    
    console.log("sortby " + sortby);
    console.log(typeof(keyword));
    if(keyword == ''){
        console.log("keyword is empty ?");
    }
    var whereClause = { offset: offset, limit: itemsPerPage, order: [['first_name', sortby], ['last_name', sortby]]};
    console.log(keyword.length);
    console.log(keyword !== 'undefined');
    console.log(keyword != undefined);
    console.log(!keyword);
    console.log(keyword.trim().length > 0);

    if((keyword !== 'undefined' || !keyword) && keyword.trim().length > 0){
        console.log("> " + keyword);
        whereClause = { offset: offset, limit: itemsPerPage, order: [['first_name', sortby], ['last_name', sortby]], where: {first_name: keyword}};
    }
    console.log(whereClause);
    Customers.findAndCountAll(whereClause).then((results)=>{
        res.status(200).json(results);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.get(EMPLOYEE_API+"/:cust_no", (req, res)=>{
    console.log("one customer ...");
    console.log(req.params.cust_no);
    var cust_no = req.params.cust_no;
    console.log(cust_no);
    var whereClause = {limit: 1, where: {cust_no: cust_no}};
    Customers.findOne(whereClause).then((result)=>{
        res.status(200).json(result);
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    });    
});

app.delete(EMPLOYEE_API+"/:cust_no", (req, res)=>{
    console.log("one customer ...");
    console.log(req.params.cust_no);
    var cust_no = req.params.cust_no;
    console.log(cust_no);
    var whereClause = {limit: 1, where: {cust_no: cust_no}};
    Customers.findOne(whereClause).then((result)=>{
        result.destroy();
        res.status(200).json({});
    }).catch((error)=>{
        console.log(error);
        res.status(500).json(error);
    }); 
});

app.use(function (req, res) {
    res.send("<h1>Page not found</h1>");
});

app.listen(node_port, function () {
    console.log("Server running at http://localhost:" + node_port);
});

module.exports = app;