CREATE TABLE `customers` (
  `cust_no` int(11) NOT NULL AUTO_INCREMENT,
  `birth_date` date NULL,
  `first_name` varchar(14) NOT NULL,
  `last_name` varchar(16) NULL,
  `gender` enum('M','F') NOT NULL,
  `join_date` date NOT NULL,
  `photourl` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`cust_no`)
) ENGINE=InnoDB AUTO_INCREMENT=4000012 DEFAULT CHARSET=latin1;

INSERT INTO `customers`.`customers` (`cust_no`, `birth_date`, `first_name`, `last_name`, `gender`, `join_date`, `photourl`) VALUES ('10001', '1967-11-12', 'Davis', 'Lim', 'M', '1986-06-26', 'https://upload.wikimedia.org/wikipedia/commons/d/de/Facebook_head.png');
