module.exports = function(connection, Sequelize){
    // customers this have to match the mysql table
    // return Customers object is used within the JS
    var Customers = connection.define('customers', {
        cust_no: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        birth_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        first_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        last_name:{
            type: Sequelize.STRING,
            allowNull: false
        },
        gender:{
            type: Sequelize.ENUM('M', 'F'),
            allowNull: false
        },
        join_date: {
            type: Sequelize.DATE,
            allowNull: false
        },
        photourl: {
            type: Sequelize.STRING,
            defaultValue: "https://upload.wikimedia.org/wikipedia/commons/d/de/Facebook_head.png"
        }
    }, {
        timestamps: false
    });
    return Customers;
}